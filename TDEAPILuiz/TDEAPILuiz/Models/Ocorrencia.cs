﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDEAPILuiz.Models
{
    public class Ocorrencia
    {
        public int Id { get; set; }

        public DateTime DataDaOcorrencia { get; set; }

        public string Descricao { get; set; }


        public List<FuncionarioOcorrencia> FuncionariosOcorrencias { get; set; }

    }
}
