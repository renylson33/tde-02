﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDEAPILuiz.Models
{
    public class Funcionario
    {

        public int Id { get; set; }

        public string Nome { get; set; }

        public DateTime DataDeNasc { get; set; }

        public string Email { get; set; }



        public Usuario Usuario { get; set; }

        public List<FuncionarioOcorrencia> FuncionariosOcorrencias { get; set; }

    }
}
