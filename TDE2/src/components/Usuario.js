import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './App.css';

export class Usuario extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      email: '',
      Usuario: [],
      modalAberta: false
    };

    this.buscarUsuario = this.buscarUsuario.bind(this);
    this.buscarusuario = this.buscarusuario.bind(this);
    this.inserirusuario = this.inserirusuario.bind(this);
    this.atualizarusuario = this.atualizarusuario.bind(this);
    this.excluirusuario = this.excluirusuario.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }

  componentDidMount() {
    this.buscarUsuario();
  }

  // GET (todos Usuario)
  buscarUsuario() {
    fetch('http://localhost:60963/api/Usuario')
      .then(response => response.json())
      .then(data => this.setState({ Usuario: data }));
  }
  
  //GET (usuario com determinado id)
  buscarusuario(id) {
    fetch('http://localhost:60963/api/Usuario/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }

  inserirusuario = (usuario) => {
    fetch('http://localhost:60963/api/Usuario', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(usuario)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarUsuario();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarusuario(usuario) {
    fetch('http://localhost:60963/api/Usuario/' + usuario.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(usuario)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarUsuario();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirusuario = (id) => {
    fetch('http://localhost:60963/api/Usuario/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarUsuario();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarusuario(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const usuario = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirusuario(usuario);
    } else {
      this.atualizarusuario(usuario);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do usuario</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do usuario' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type='email' placeholder='Email' value={this.state.email} onChange={this.atualizaEmail} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Nome do usuario</th>
            <th>E-mail</th>
          </tr>
        </thead>
        <tbody>
          {this.state.Usuario.map((usuario) => (
            <tr key={usuario.id}>
              <td>{usuario.nome}</td>
              <td>{usuario.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(usuario.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirusuario(usuario.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar usuario</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}