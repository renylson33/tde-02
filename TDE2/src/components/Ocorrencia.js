import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './App.css';

export class Ocorrencia extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      numero: '',
      Ocorrencia: [],
      modalAberta: false
    };

    this.buscarOcorrencia = this.buscarOcorrencia.bind(this);
    this.buscarocorrencia = this.buscarocorrencia.bind(this);
    this.inserirocorrencia = this.inserirocorrencia.bind(this);
    this.atualizarocorrencia = this.atualizarocorrencia.bind(this);
    this.excluirocorrencia = this.excluirocorrencia.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
    this.atualizaNumero = this.atualizaNumero.bind(this);
  }

  componentDidMount() {
    this.buscarOcorrencia();
  }

  // GET (todos Ocorrencia)
  buscarOcorrencia() {
    fetch('http://localhost:60963/api/Ocorrencia')
      .then(response => response.json())
      .then(data => this.setState({ Ocorrencia: data }));
  }
  
  //GET (ocorrencia com determinado id)
  buscarocorrencia(id) {
    fetch('http://localhost:60963/api/Ocorrencia/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }

  inserirocorrencia = (ocorrencia) => {
    fetch('http://localhost:60963/api/Ocorrencia', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(ocorrencia)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarOcorrencia();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarocorrencia(ocorrencia) {
    fetch('http://localhost:60963/api/Ocorrencia/' + ocorrencia.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(ocorrencia)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarOcorrencia();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirocorrencia = (id) => {
    fetch('http://localhost:60963/api/Ocorrencia/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarOcorrencia();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaNumero(e) {
    this.setState({
      numero: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarocorrencia(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const ocorrencia = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirocorrencia(ocorrencia);
    } else {
      this.atualizarocorrencia(ocorrencia);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do ocorrencia</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do ocorrencia' value={this.state.nome} onChange={this.atualizaNome} />
              <Form.Label>Prioridade</Form.Label>
              <Form.Control type='number' placeholder='Numero da ocorrencia' value={this.state.Numero} onChange={this.atualizaNumero} />
            </Form.Group>
            <Form.Group>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Nome</th>
            <th>Prioridade</th>
          </tr>
        </thead>
        <tbody>
          {this.state.Ocorrencia.map((ocorrencia) => (
            <tr key={ocorrencia.id}>
              <td>{ocorrencia.nome}</td>
              <td>{ocorrencia.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(ocorrencia.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirocorrencia(ocorrencia.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar ocorrencia</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}