import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './App.css';

export class Departamentos extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      email: '',
      Departamentos: [],
      modalAberta: false
    };

    this.buscarDepartamentos = this.buscarDepartamentos.bind(this);
    this.buscardepartamentos = this.buscardepartamentos.bind(this);
    this.inserirdepartamentos = this.inserirdepartamentos.bind(this);
    this.atualizardepartamentos = this.atualizardepartamentos.bind(this);
    this.excluirdepartamentos = this.excluirdepartamentos.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }

  componentDidMount() {
    this.buscarDepartamentos();
  }

  // GET (todos Departamentos)
  buscarDepartamentos() {
    fetch('http://localhost:60963/api/Departamentos')
      .then(response => response.json())
      .then(data => this.setState({ Departamentos: data }));
  }
  
  //GET (departamentos com determinado id)
  buscardepartamentos(id) {
    fetch('http://localhost:60963/api/Departamentos/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }

  inserirdepartamentos = (departamentos) => {
    fetch('http://localhost:60963/api/Departamentos', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(departamentos)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarDepartamentos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizardepartamentos(departamentos) {
    fetch('http://localhost:60963/api/Departamentos/' + departamentos.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(departamentos)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarDepartamentos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirdepartamentos = (id) => {
    fetch('http://localhost:60963/api/Departamentos/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarDepartamentos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscardepartamentos(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const departamentos = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirdepartamentos(departamentos);
    } else {
      this.atualizardepartamentos(departamentos);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do departamentos</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do departamentos' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Nome do departamento</th>
          </tr>
        </thead>
        <tbody>
          {this.state.Departamentos.map((departamentos) => (
            <tr key={departamentos.id}>
              <td>{departamentos.nome}</td>
              <td>{departamentos.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(departamentos.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirdepartamentos(departamentos.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar departamentos</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}